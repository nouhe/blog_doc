<?php
/**
 * Created by PhpStorm.
 * User: Nouhe
 * Date: 04/06/2019
 * Time: 02:22
 */

namespace AppBundle\Services;


use Doctrine\ORM\EntityManager;

class ArticleService
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $connection;

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
        $this->connection = $entityManager->getConnection();
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        $select = $this->connection->prepare('SELECT id, title, description FROM articles');
        $select->execute();

        return $select->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param int $iId id d'article
     * @return mixed
     */
    public function getArticleById($iId)
    {
        $select = $this->connection->prepare('SELECT id, title, description FROM articles where id = :id');
        $select->bindParam('id', $iId);
        $select->execute();

        return $select->fetch(\PDO::FETCH_ASSOC);
    }

    public function addArticle($aInfoArticle)
    {
        $oStatment = $this->connection->prepare('INSERT INTO articles (title, description) VALUES (:title, :description)');
        $oStatment->bindParam('title', $aInfoArticle['title']);
        $oStatment->bindParam('description', $aInfoArticle['description']);

        $oStatment->execute();
    }

}