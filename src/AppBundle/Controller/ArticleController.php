<?php
/**
 * Created by PhpStorm.
 * User: Nouhe
 * Date: 04/06/2019
 * Time: 02:14
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package AppBundle\Controller
 * @Route("/blog")
 */
class ArticleController extends Controller
{

    /**
     * afficher la liste des articles
     *
     * @Route("/articles", name="article_index")
     * @return Response
     */
    public function indexAction(): Response
    {
        $aArticles = $this->get('app.article.service')->getArticles();

        return $this->render('@App/Article/index.html.twig', compact('aArticles'));
    }

    /**
     * afficher les détails d'un article
     *
     * @Route ("/article/{iId}", name="article_detail")
     * @param int $iId id d'article
     * @return Response
     */
    public function detailsAction($iId = null): Response
    {
        if(!is_numeric($iId)) {
            return new Response('failure');
        }

        $article = $this->get('app.article.service')->getArticleById($iId);

        if($article === false) {
            $this->addFlash('info', 'Article introuvable');
            return $this->redirectToRoute('article_index');
        }


        return $this->render('@App/Article/details.html.twig', compact('article'));
    }

}