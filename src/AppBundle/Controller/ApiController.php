<?php
/**
 * Created by PhpStorm.
 * User: Nouhe
 * Date: 04/06/2019
 * Time: 11:34
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 * @package AppBundle\Controller
 * @Route("/api")
 */
class ApiController extends Controller
{

    /**
     * afficher tous les articles
     *
     * @Route("/articles")
     * @return JsonResponse
     */
    public function allArticlesAction(): JsonResponse
    {
        $aArticles = $this->get('app.article.service')->getArticles();

        return $this->json(['data' => $aArticles]);
    }

    /**
     * afficher un article par id
     *
     * @Route("/article/{iId}")
     * @return JsonResponse
     */
    public function showArticleAction($iId): JsonResponse
    {
        if(!is_numeric($iId)) {
            return $this->json(['message' => 'failure'], 422);
        }

        $article = $this->get('app.article.service')->getArticleById($iId);

        return $this->json(['data' => $article]);
    }

    /**
     * @Route("/article", methods= {"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addArticleAction(Request $request): JsonResponse
    {
        if($request->getMethod() === 'POST') {

            if(!$request->request->has('title') || !$request->request->has('description')) {
                return $this->json(['message' => 'parametres non valides'], 422);
            }

            $this->get('app.article.service')->addArticle($request->request->all());

            return $this->json(['message' => 'success']);
        } else {
            return $this->json(['message' => 'Methode non autorisée']);
        }
    }

}